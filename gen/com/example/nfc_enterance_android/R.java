/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.nfc_enterance_android;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int aqua=0x7f08000a;
        public static final int black=0x7f080010;
        public static final int blue=0x7f08000e;
        public static final int bright_gray=0x7f080005;
        public static final int fuchsia=0x7f080002;
        public static final int gray=0x7f080006;
        public static final int green=0x7f08000d;
        public static final int lime=0x7f08000b;
        public static final int maroon=0x7f080009;
        public static final int navy=0x7f08000f;
        public static final int olive=0x7f080007;
        public static final int purple=0x7f080008;
        public static final int red=0x7f080003;
        public static final int silver=0x7f080004;
        public static final int teal=0x7f08000c;
        public static final int white=0x7f080000;
        public static final int yellow=0x7f080001;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f050000;
        public static final int activity_vertical_margin=0x7f050001;
    }
    public static final class drawable {
        public static final int ic_launcher=0x7f020000;
        public static final int unknown_person=0x7f020001;
    }
    public static final class id {
        public static final int action_settings=0x7f0a0047;
        public static final int category=0x7f0a0043;
        public static final int categoryEntry=0x7f0a0045;
        public static final int categoryLabel=0x7f0a0044;
        public static final int deleteBtn=0x7f0a002d;
        public static final int department=0x7f0a0040;
        public static final int departmentEntry=0x7f0a0042;
        public static final int departmentLabel=0x7f0a0041;
        public static final int editBtn=0x7f0a002c;
        public static final int exitRadio01=0x7f0a000e;
        public static final int exitRadio02=0x7f0a000f;
        public static final int exitRadioGroup=0x7f0a000d;
        public static final int exportButtonArea=0x7f0a0019;
        public static final int exportFromEntry=0x7f0a000a;
        public static final int exportInputForm=0x7f0a0007;
        public static final int exportItemEntity=0x7f0a0008;
        public static final int exportMemberName=0x7f0a0009;
        public static final int exportPictureEntity=0x7f0a000b;
        public static final int exportReason=0x7f0a0016;
        public static final int exportReasonEntry=0x7f0a0018;
        public static final int exportReasonLabel=0x7f0a0017;
        public static final int exportTitleLabel=0x7f0a001a;
        public static final int exportTitleLabel1=0x7f0a0005;
        public static final int exportTitleLabel2=0x7f0a0006;
        public static final int importDateEntry=0x7f0a000c;
        public static final int inquiry_header=0x7f0a001d;
        public static final int inquiry_header1=0x7f0a001e;
        public static final int inquiry_header2=0x7f0a001f;
        public static final int inquiry_header3=0x7f0a0020;
        public static final int inquiry_header4=0x7f0a0021;
        public static final int inquiry_scroll=0x7f0a001b;
        public static final int inquiry_table=0x7f0a001c;
        public static final int login_button=0x7f0a0024;
        public static final int login_id=0x7f0a0022;
        public static final int login_pass=0x7f0a0023;
        public static final int mainManagerEntry=0x7f0a003d;
        public static final int mainManagerLabel=0x7f0a003c;
        public static final int manager=0x7f0a003a;
        public static final int managerLabel=0x7f0a003b;
        public static final int name=0x7f0a0031;
        public static final int nameEntry=0x7f0a0033;
        public static final int nameLabel=0x7f0a0032;
        public static final int newRegBtn=0x7f0a002b;
        public static final int newRegOkButton=0x7f0a0046;
        public static final int newRegTitleLabel=0x7f0a002e;
        public static final int nfcRead=0x7f0a0025;
        public static final int okBtn=0x7f0a0027;
        public static final int price=0x7f0a0037;
        public static final int priceEntry=0x7f0a0039;
        public static final int priceLabel=0x7f0a0038;
        public static final int purchaseDate=0x7f0a0034;
        public static final int purchaseDateEntry=0x7f0a0036;
        public static final int purchaseDateLabel=0x7f0a0035;
        public static final int regLabel=0x7f0a002a;
        public static final int serialNumber=0x7f0a002f;
        public static final int serialNumberEntry=0x7f0a0026;
        public static final int serialNumberLabel=0x7f0a0030;
        public static final int serialNumberLabel01=0x7f0a0028;
        public static final int serialNumberLabel02=0x7f0a0029;
        public static final int titleLabel1=0x7f0a0000;
        public static final int titleLabel2=0x7f0a0003;
        public static final int titleLabelSub1=0x7f0a0001;
        public static final int titleLabelSub2=0x7f0a0002;
        public static final int titleMenuTable=0x7f0a0004;
        public static final int viceManagerEntry=0x7f0a003f;
        public static final int viceManagerLabel=0x7f0a003e;
        public static final int whoseRadio=0x7f0a0010;
        public static final int whoseRadio01=0x7f0a0013;
        public static final int whoseRadio02=0x7f0a0014;
        public static final int whoseRadio03=0x7f0a0015;
        public static final int whoseRadioGroup=0x7f0a0012;
        public static final int whoseRadioLabel=0x7f0a0011;
    }
    public static final class layout {
        public static final int activity_main=0x7f030000;
        public static final int export_request_activity=0x7f030001;
        public static final int inquiry_activity=0x7f030002;
        public static final int log_in_activity=0x7f030003;
        public static final int nfc_read_activity=0x7f030004;
        public static final int nfc_write_activity=0x7f030005;
        public static final int reg_activity=0x7f030006;
        public static final int reg_new_activity=0x7f030007;
    }
    public static final class menu {
        public static final int main=0x7f090000;
    }
    public static final class string {
        public static final int TEXT_AUTHORITY_NO=0x7f060024;
        public static final int TEXT_COMMUNICATION=0x7f060070;
        public static final int TEXT_COMMUNICATION_MESSAGE=0x7f060071;
        public static final int TEXT_ENROLL_DELETE_RESULT_SUCCESS=0x7f060031;
        public static final int TEXT_ENROLL_MODIFY_RESULT_SUCCESS=0x7f060030;
        public static final int TEXT_ENROLL_RESULT_SUCCESS=0x7f060027;
        public static final int TEXT_EXORRT_CANCLE_RESULT_FAIL=0x7f06002c;
        public static final int TEXT_EXPORT_APPROVE_RESULT_SUCCESS=0x7f06002e;
        public static final int TEXT_EXPORT_CANCLE=0x7f06002a;
        public static final int TEXT_EXPORT_CANCLE_RESULT_SUCCESS=0x7f06002b;
        public static final int TEXT_EXPORT_MODIFY_RESULT_SUCCESS=0x7f06002f;
        public static final int TEXT_EXPORT_REQUEST_RESULT_SUCCESS=0x7f06002d;
        public static final int TEXT_EXPORT_RESULT_ALREADY=0x7f060025;
        public static final int TEXT_IMPORT_RESULT_ALREADY=0x7f060033;
        public static final int TEXT_IMPORT_RESULT_NOT_APPROVED=0x7f060034;
        public static final int TEXT_IMPORT_RESULT_SUCCESS=0x7f060032;
        public static final int TEXT_INQUIRY_COMMAND_CHOOSE_KEY=0x7f060039;
        public static final int TEXT_INQUIRY_COMMAND_ENTER_SEARCH=0x7f06003a;
        public static final int TEXT_INQUIRY_DELETED=0x7f06003b;
        public static final int TEXT_INQUIRY_DETAIL=0x7f06005e;
        public static final int TEXT_INQUIRY_HEADER_DATE=0x7f060036;
        public static final int TEXT_INQUIRY_HEADER_MEMBER=0x7f060038;
        public static final int TEXT_INQUIRY_HEADER_STATE=0x7f060035;
        public static final int TEXT_INQUIRY_HEADER_UNIT=0x7f060037;
        public static final int TEXT_KEY_ALL=0x7f06000f;
        public static final int TEXT_KEY_APPROVE=0x7f060016;
        public static final int TEXT_KEY_CANCLE=0x7f06001e;
        public static final int TEXT_KEY_DATE=0x7f060010;
        public static final int TEXT_KEY_DELETE=0x7f060019;
        public static final int TEXT_KEY_ENROLL=0x7f06000b;
        public static final int TEXT_KEY_EXPORT=0x7f06000d;
        public static final int TEXT_KEY_EXPORT_APPROVE=0x7f060013;
        public static final int TEXT_KEY_EXPORT_REQUEST=0x7f060012;
        public static final int TEXT_KEY_EXPORT_WAIT=0x7f060011;
        public static final int TEXT_KEY_FORM_EXITVALUE=0x7f06005a;
        public static final int TEXT_KEY_FORM_EXITVALUE_EXIT=0x7f060020;
        public static final int TEXT_KEY_FORM_EXITVALUE_FRONT=0x7f06001f;
        public static final int TEXT_KEY_FORM_IMPORTDATE=0x7f060058;
        public static final int TEXT_KEY_FORM_IMPORTDATE_HINT=0x7f060059;
        public static final int TEXT_KEY_FORM_REASON=0x7f06005c;
        public static final int TEXT_KEY_FORM_REASON_HINT=0x7f06005d;
        public static final int TEXT_KEY_FORM_STATE=0x7f060055;
        public static final int TEXT_KEY_FORM_TO=0x7f060056;
        public static final int TEXT_KEY_FORM_TO_HINT=0x7f060057;
        public static final int TEXT_KEY_FORM_WHOSE=0x7f06005b;
        public static final int TEXT_KEY_FORM_WHOSE_ABROAD=0x7f060022;
        public static final int TEXT_KEY_FORM_WHOSE_COMPANY=0x7f060021;
        public static final int TEXT_KEY_FORM_WHOSE_ETC=0x7f060023;
        public static final int TEXT_KEY_IMPORT=0x7f06000e;
        public static final int TEXT_KEY_IMPORTED=0x7f060015;
        public static final int TEXT_KEY_INQUIRY=0x7f06000c;
        public static final int TEXT_KEY_LOGIN=0x7f06001b;
        public static final int TEXT_KEY_LOGOUT=0x7f06001c;
        public static final int TEXT_KEY_MEMBER_NAME=0x7f060050;
        public static final int TEXT_KEY_MEMBER_NUMBER=0x7f060051;
        public static final int TEXT_KEY_MEMBER_PART=0x7f060052;
        public static final int TEXT_KEY_MEMBER_PHONE=0x7f060053;
        public static final int TEXT_KEY_MEMBER_PICTURE=0x7f060054;
        public static final int TEXT_KEY_MODIFY=0x7f060017;
        public static final int TEXT_KEY_NEWENROLL=0x7f060018;
        public static final int TEXT_KEY_NOT_IMPORTED=0x7f060014;
        public static final int TEXT_KEY_OK=0x7f06001d;
        public static final int TEXT_KEY_TAKEFORM=0x7f06001a;
        public static final int TEXT_KEY_UNIT_CATEGORY=0x7f060047;
        public static final int TEXT_KEY_UNIT_CATEGORY_HINT=0x7f060048;
        public static final int TEXT_KEY_UNIT_MANAGER=0x7f060040;
        public static final int TEXT_KEY_UNIT_MANAGER_PRI=0x7f060041;
        public static final int TEXT_KEY_UNIT_MANAGER_PRI_HINT=0x7f060042;
        public static final int TEXT_KEY_UNIT_MANAGER_SUB=0x7f060043;
        public static final int TEXT_KEY_UNIT_MANAGER_SUB_HINT=0x7f060044;
        public static final int TEXT_KEY_UNIT_NAME=0x7f06003c;
        public static final int TEXT_KEY_UNIT_NAME_HINT=0x7f06003d;
        public static final int TEXT_KEY_UNIT_NFCID=0x7f06004f;
        public static final int TEXT_KEY_UNIT_NFCID_BUTTON=0x7f06004b;
        public static final int TEXT_KEY_UNIT_NFCID_NOT=0x7f06004c;
        public static final int TEXT_KEY_UNIT_NUMBER=0x7f060049;
        public static final int TEXT_KEY_UNIT_NUMBER_HINT=0x7f06004a;
        public static final int TEXT_KEY_UNIT_PART=0x7f060045;
        public static final int TEXT_KEY_UNIT_PART_HINT=0x7f060046;
        public static final int TEXT_KEY_UNIT_PRICE=0x7f06003e;
        public static final int TEXT_KEY_UNIT_PRICE_HINT=0x7f06003f;
        public static final int TEXT_KEY_UNIT_PURCHASE_DATE=0x7f06004d;
        public static final int TEXT_KEY_UNIT_PURCHASE_DATE_HINT=0x7f06004e;
        public static final int TEXT_LOGIN_BUTTON=0x7f060063;
        public static final int TEXT_LOGIN_ID=0x7f06005f;
        public static final int TEXT_LOGIN_ID_HINT=0x7f060061;
        public static final int TEXT_LOGIN_PASSWORD=0x7f060060;
        public static final int TEXT_LOGIN_PASSWORD_HINT=0x7f060062;
        public static final int TEXT_LOGIN_RESULT_WRONG_ID=0x7f060065;
        public static final int TEXT_LOGIN_RESULT_WRONG_PASSWORD=0x7f060066;
        public static final int TEXT_LOGIN_WELCOME=0x7f060064;
        public static final int TEXT_LOGOUT_RESULT_SUCCESS=0x7f060067;
        public static final int TEXT_NFC_FAIL_AUTHORITY=0x7f06006e;
        public static final int TEXT_NFC_FAIL_GENERAL=0x7f06006c;
        public static final int TEXT_NFC_FAIL_LONG=0x7f06006d;
        public static final int TEXT_NFC_TAG_MESSAGE=0x7f060068;
        public static final int TEXT_NFC_UNIT_NUMBER_HINT=0x7f060069;
        public static final int TEXT_NFC_UNIT_NUMBER_MESSAGE1=0x7f06006a;
        public static final int TEXT_NFC_UNIT_NUMBER_MESSAGE2=0x7f06006b;
        public static final int TEXT_RESULT_UNKNOWN=0x7f06006f;
        public static final int TEXT_TAG_ENROLL_ALREADY=0x7f060028;
        public static final int TEXT_TAG_ENROLL_RESULT_SUCCESS=0x7f060026;
        public static final int TEXT_TAG_MODIFY_RESULT_SUCCESS=0x7f060029;
        public static final int TEXT_TITLE_ENROLL=0x7f060002;
        public static final int TEXT_TITLE_ENROLL_MODIFY=0x7f060004;
        public static final int TEXT_TITLE_ENROLL_NEW=0x7f060003;
        public static final int TEXT_TITLE_EXPORT_APPROVE=0x7f060008;
        public static final int TEXT_TITLE_EXPORT_MODIFY=0x7f060007;
        public static final int TEXT_TITLE_EXPORT_MODIFY_APPROVE=0x7f060006;
        public static final int TEXT_TITLE_EXPORT_NEW=0x7f060005;
        public static final int TEXT_TITLE_IMPORT=0x7f060009;
        public static final int TEXT_TITLE_INQUIRY=0x7f06000a;
        public static final int action_settings=0x7f060001;
        public static final int app_name=0x7f060000;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f070000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f070001;
    }
    public static final class xml {
        public static final int nfc_tech_filter=0x7f040000;
    }
}
