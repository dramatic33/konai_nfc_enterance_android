KONAI_NFC_Enterance_Android
===========================

Query 실행에 변경사항이 있습니다. 대기 화면을 표시하기 위해 쓰레드가 본 프로세스와 전혀 다른 라인으로 진행하시 위해 바꾸었습니다.
Query를 실행하기 전에 대기화면을 보이면서 Query를 수행하기 위한 정보를 모으는 getInformation과 Query 통신을 마친후에 어떤식으로 결과값을 처리할지를 정해야 합니다.
오버라이드해서 구현해주세요.
통신을 마치면 결과값은 query의 getResult()로 확인하면 됩니다.
이런식으로 적용하면됩니다.

```
import insertQuery;

insertQuery insert = new insertQuery(Context){
	public void getInformation(){
		this.setName("~");
		this.setManager("~");
	}
	public void postprocess(){
		JSONObject json = new JSONObject(this.getResult());
		String result = json.getString("result");
		System.out.println(result);
	}
};
insert.start();
```

쓰레드이기 때문에 반드시 start를 해 주셔야 합니다.