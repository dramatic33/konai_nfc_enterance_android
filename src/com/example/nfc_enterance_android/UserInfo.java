package com.example.nfc_enterance_android;

public class UserInfo {
	private int mNo;
	private String authCode;
	public UserInfo(int mNo, String authCode){
		this.mNo=mNo;
		this.authCode=authCode;
	}
	public int getmNo(){
		return this.mNo;
	}
	public boolean canEnroll(){
		return authCode.charAt(0)=='1';
	}
	public boolean canInquiry(){
		return authCode.charAt(1)=='1';
	}
	public boolean canExport(){
		return authCode.charAt(2)=='1';
	}
	public boolean canImport(){
		return authCode.charAt(3)=='1';
	}
	public boolean canApproveExport(){
		return authCode.charAt(4)=='1';
	}
}
