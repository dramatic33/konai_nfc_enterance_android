package com.example.nfc_enterance_android;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.os.Bundle;
import android.util.Log;

abstract public class NFCProcess extends Activity {
	private NfcAdapter mnfc;
	private PendingIntent pending;
	private IntentFilter[] IntentFilterArray;
	private String[][] techList;
	protected MifareClassic classic;
	protected long nfcID = 0;
	protected int targetSecter = 11;
	private AlertDialog.Builder builder;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    mnfc = NfcAdapter.getDefaultAdapter(this);
		pending = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter mifare = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
		 try {
             mifare.addDataType("*/*");
             IntentFilterArray = new IntentFilter[]{mifare};
		 } catch (MalformedMimeTypeException e) {
             Log.d("ERR", "mifare Type Error", e);
		}
		techList = new String[][]{new String[]{MifareClassic.class.getName()}};
	}
	public void onResume(){
		super.onResume();
		mnfc.enableForegroundDispatch(this, pending, IntentFilterArray, techList);
	}
	public void onPause(){
		super.onPause();
		mnfc.disableForegroundDispatch(this);
	}
	//Print Message and destroy activity
		protected void exitMessage(String Message){
			builder = new AlertDialog.Builder(this);
			builder.setMessage(Message).setPositiveButton(getResources().getString(R.string.TEXT_KEY_OK), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					NFCProcess.this.finish();					
				}
			});
			builder.create().show();
		}
		//Encode Message to 16 byte -> Mifare classic 1 block
		protected byte[] encodeMessage(MifareClassic classic, String input){
			byte[] encode = input.getBytes(Charset.forName("utf-8"));
			int BlockSize = 16;
			byte[] inputstream = new byte[BlockSize];
			if(encode.length>BlockSize)
				exitMessage(getResources().getString(R.string.TEXT_NFC_FAIL_LONG));
			for(int i=0;i<BlockSize;i++){
				if(i<encode.length){
					inputstream[i]=encode[i];
				} else {
					inputstream[i]=0;
				}
			}
			return inputstream;
		}
		//On Detect tag in foreground
		public void onNewIntent(Intent intent){
			detectProcess(intent);
			this.returnTo();
			finish();
		}
		@SuppressWarnings("static-access")
		private void detectProcess(Intent intent){
			//Get Mifare Classic tag from intent
			Tag tag = intent.getParcelableExtra(mnfc.EXTRA_TAG);
			ByteBuffer buffer = ByteBuffer.wrap(tag.getId());
			nfcID = buffer.getInt() & 0xFFFFFFFFL;
		}
		abstract void returnTo();
}
