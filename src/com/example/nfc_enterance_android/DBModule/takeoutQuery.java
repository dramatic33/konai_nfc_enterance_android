package com.example.nfc_enterance_android.DBModule;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;


abstract public class takeoutQuery extends takeQuery{
	private String export_to;
	private String import_date;
	private String exit;
	private String whose;
	private String reason;
	private int take_mode;
	private int fNo;
	
	public takeoutQuery(Activity Context, long uNo, int mNo){
		super(Context, uNo, mNo);
		this.export_to="";
		this.import_date="0000-00-00";
		this.exit="정문";
		this.whose="회사물품";
		this.reason="";
		
		this.take_mode=0;
		this.fNo=0;
	}
	public void setuNo(int uNo){
        this.uNo=uNo;
	}
	public void setExport_to(String export_to){
        this.export_to=export_to;
	}
	public void setImport_date(String import_date){
        this.import_date=import_date;
	}
	public void setExit(String exit){
        this.exit = exit;
	}
	public void setWhose(String whose){
        this.whose=whose;
	}
	public void setReason(String reason){
        this.reason=reason;
	}
	public void setMode(int take_mode){
		this.take_mode=take_mode;
	}
	public void setfNo(int fNo){
		this.fNo=fNo;
	}
	public int getfNo(){
		return this.fNo;
	}
	public int getMode(){
		return this.take_mode;
	}
	public String execution() throws Exception{
		this.clear();
		
		post("mode", "TAKE_OUT");
		post("uNo", uNo);
		post("mNo", mNo);
		post("export_to",	export_to);
		post("import_date",	import_date);
		post("exit",		exit);
		post("whose",		whose);
		post("reason",		reason);
		
		post("takemode", Integer.valueOf(take_mode));
		post("fNo", Integer.valueOf(fNo));
		
		String result = this.request();
		
		JSONObject json = new JSONObject(result);
		return json.getString("result");
	}
	public void setPicture(JSONObject json){
		String PictureFile = null;
		try{
			PictureFile = json.getString("mNo")+".jpg";
		}catch(JSONException e){
			PictureFile = mNo+".jpg";
			//e.printStackTrace();
		}
		String URL = "http://mimosa.snu.ac.kr/~dusdk11/picture/"+PictureFile;
		this.pictre = this.getBitmapFromURL(URL);
	}
}
