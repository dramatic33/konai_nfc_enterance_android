package com.example.nfc_enterance_android.DBModule;

import android.app.Activity;

abstract public class logQuery extends query{
	
	private String key;
	private String value;
	private int rownumber;
	private int page; 
	
	public logQuery(Activity Context){
		super(Context);
		this.key="";
		this.value="";
		this.rownumber = 15;
		this.page = 1;
	}
	public void setKey(String key){
		this.key=key;
	}
	public void setValue(String value){
		this.value=value;
	}
	public void setRowNumber(int rownumber){
		this.rownumber=rownumber;
	}
	public void setPage(int page){
		this.page=page;
	}
	public String execution() throws Exception {
		this.clear();
		post("mode", "LOG");
		post("row_num", rownumber);
		post("page", page);
		post("key", key);
		post("value", value);
		
		return this.request();
	}
	public int getRownum(){
		return rownumber;
	}

}
