package com.example.nfc_enterance_android.DBModule;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;

abstract public class takeinQuery extends takeQuery{
	public takeinQuery( Activity Context, long uNo, int mNo){
		super(Context, uNo, mNo);
		this.uNo=uNo;
	}
	@Override
	public String execution() throws Exception {
		this.clear();
		
		post("mode", "TAKE_IN");
		post("unit_no", uNo);
		post("member_no", mNo);
		post("fNo", fNo);
		
		String result = this.request();
		JSONObject json = new JSONObject(result);
		return json.getString("result");
	}
	public void setPicture(JSONObject json){
		try{
			String mNo = json.getString("mNo");
			String URL = "http://mimosa.snu.ac.kr/~dusdk11/picture/"+mNo+".jpg";
			this.pictre = this.getBitmapFromURL(URL);
		}catch(JSONException e){
			e.printStackTrace();
		}

	}
}
