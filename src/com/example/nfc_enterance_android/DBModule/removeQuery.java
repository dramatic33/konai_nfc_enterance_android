package com.example.nfc_enterance_android.DBModule;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;

abstract public class removeQuery extends query{
	String type;
	String value;
	public removeQuery(Activity Context){
		super(Context);
	}
	public void setType(String type){
		this.type=type;
	}
	public void setValue(String value){
		this.value=value;
	}
	public String execution() throws JSONException{
		this.clear();
		this.post("mode", "DELETE");
		this.post("type", type);
		this.post("value", value);
		
		String result = this.request();
		JSONObject json = new JSONObject(result);
		return json.getString("result");
	}
}
