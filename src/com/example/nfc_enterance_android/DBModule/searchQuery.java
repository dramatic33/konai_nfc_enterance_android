package com.example.nfc_enterance_android.DBModule;

import android.app.Activity;


abstract public class searchQuery extends query{
	public static enum table {unit, member, log, take_form};
	public static enum unit_key {uNo, uName, In_date, price, manager, sub_manager, part, serialNumber};
	public static enum mem_key {mNo, mName, pNo, auth_ID};
	public static enum log_key {iNo, flag, date, unit_no, member_no};
	public static enum take_key {fNo, uNo, mNo, iNo, export_to, import_date, exit, whose};
	private String table;
	private String key;
	private String type;
	private String value;
	public searchQuery(Activity Context){
		super(Context);
		this.table=null;
		this.key = null;
		this.value=null;
	}
	public void setTable(table table){
		this.table=table.name();
	}
	public void setUnitKey(unit_key key){
		this.key=key.name();
		switch(key){
		case uNo:
		case price: this.type="INTEGER"; break;
		case In_date: this.type="DATE"; break;
		default: this.type="VARCHAR"; break;
		}
	}
	public void setMemberKey(mem_key key){
		this.key=key.name();
		switch(key){
		case mName: this.type="VARCHAR"; break;
		default: this.type="INTEGER"; break;
		}
	}
	public void setLogKey(log_key key){
		this.key=key.name();
		switch(key){
		case flag: this.type="VARCHAR"; break;
		case date: this.type="DATE"; break;
		default: this.type="INTEGER"; break;
		}
	}
	public void setTakeKey(take_key key){
		this.key=key.name();
		switch(key){	
		case import_date: this.type="DATE"; break;
		case export_to:
		case exit:
		case whose: this.type="VARCHAR"; break;
		default: this.type="INTEGER"; break;
		}
	}
	public void setValue(String value){
		this.value=value;
	}
	public String execution() throws Exception {
		this.clear();
		if(table==null||key==null||value==null) throw new Exception("NULL Value");
		
		post("mode", "SEARCH");
		post("table", this.table);
		post("key", this.key);
		post("type", this.type);
		post("value", this.value);
		
		return this.request();
	}

}

