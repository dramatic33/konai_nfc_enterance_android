package com.example.nfc_enterance_android.DBModule;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

abstract public class LogDetailQuery extends query{
	private String type;
	private String iNo;
	private Bitmap picture;
	public LogDetailQuery(Activity Context){
		super(Context);
	}
	
	public void setType(String type){
		this.type=type;
	}
	public void setiNo(String iNo){
		this.iNo=iNo;
	}
	public String execution() throws Exception {
		this.clear();
		post("mode", "LOG_DETAIL");
		post("type", type);
		post("iNo", iNo);
		
		return this.request();
	}
	public Bitmap getPicrue(){
		return this.picture;
	}
	public void run(){
		try {
			this.getInformation();
			this.result = this.execution();
			if(this.result.compareTo("false\n")!=0){
				JSONObject json = new JSONObject(this.result);
				if(type.compareTo("member")==0){
					this.picture = getBitmapFromURL("http://mimosa.snu.ac.kr/~dusdk11/picture/"+json.getString("mNo")+".jpg");
				}
			}
			handler.sendMessage(handler.obtainMessage());
		} catch (Exception e) {
			handler.sendMessage(handler.obtainMessage());
			e.printStackTrace();
		}
	}
	private Bitmap getBitmapFromURL(String src) {
	      HttpURLConnection connection = null;
	      try {
	          URL url = new URL(src);
	          connection = (HttpURLConnection) url.openConnection();
	          connection.setDoInput(true);
	          connection.connect();
	          InputStream input = connection.getInputStream();
	          BitmapFactory.Options option = new BitmapFactory.Options();
	          option.inSampleSize = 2;
	          option.inPreferQualityOverSpeed = Bitmap.Config.ARGB_8888 != null;
	          Bitmap myBitmap = BitmapFactory.decodeStream(input, null, option);
	          if(connection!=null)connection.disconnect();
	          return myBitmap;
	      } catch (IOException e) {
	    	  e.printStackTrace();
	    	  if(connection!=null)connection.disconnect();
	          return null;
	      }
	}
}
