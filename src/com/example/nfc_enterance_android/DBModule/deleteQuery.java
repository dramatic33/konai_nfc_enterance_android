package com.example.nfc_enterance_android.DBModule;

import org.json.JSONObject;

import android.app.Activity;

abstract public class deleteQuery extends query{
	int uNo;
	public deleteQuery(Activity Context){
		super(Context);
		this.uNo=0;
	}
	public void setuNo(int uNo){
		this.uNo=uNo;
	}
	public String execution() throws Exception {
		this.clear();
		post("mode", "UDELETE");
		if(uNo==0) throw new Exception("Enter unit Number");
		post("uNo", uNo);
		
		JSONObject json = new JSONObject(this.request());
		return json.getString("result");
	}
}
