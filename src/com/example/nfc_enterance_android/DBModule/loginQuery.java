package com.example.nfc_enterance_android.DBModule;

import android.app.Activity;

abstract public class loginQuery extends query{

	String ID;
	String Password;
	
	public loginQuery(Activity Context){
		super(Context);
		ID=null;
		Password=null;
	}
	public void setID(String ID){
		this.ID=ID;
	}
	public void setPassword(String Password){
		this.Password=Password;
	}
	public String execution() throws Exception {
		this.clear();
		post("mode", "LOG_IN");
		post("ID", ID);
		post("password", Password);
		
		return this.request();
	}

}
