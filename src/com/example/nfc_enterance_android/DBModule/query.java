package com.example.nfc_enterance_android.DBModule;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Message;

abstract public class query extends Thread{
	private ArrayList<NameValuePair> list;
	private ProgressDialog dialog;
	protected String result;
	public query(Activity Context){
		this.list = new ArrayList<NameValuePair>();
		makeDialog(Context);
	}
	public query(){
		this.list = new ArrayList<NameValuePair>();
	}
	protected void makeDialog(Activity Context){
		dialog = new ProgressDialog(Context);
		dialog.setTitle("통신중");
		dialog.setMessage("잠시만 기다려 주세요");
		dialog.setIndeterminate(true);
		dialog.setCancelable(true);
		dialog.show();	
	}
	protected void dismissDialog(){
		this.dialog.dismiss();
	}
	public String getResult(){
		return this.result;
	}
	protected void post(String key, String value){
		byte[] decode = value.getBytes();
		try {
			this.list.add(new BasicNameValuePair(key, new String(decode, "utf-8")));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void post(String key, int value){
		this.list.add(new BasicNameValuePair(key, Integer.toString(value)));
	}
	protected void post(String key, long value){
		this.list.add(new BasicNameValuePair(key, Long.toString(value)));
	}
	protected void clear(){
		this.list.clear();
	}
	abstract public void getInformation();
	abstract public void postprocess();
	abstract public String execution() throws Exception;
	protected Handler handler = new Handler(){
		public void handleMessage(Message handle){
			if(dialog!=null) dialog.dismiss();
			postprocess();
		}
	};
	public void run(){
		try {
			this.getInformation();
			this.result = this.execution();
			handler.sendMessage(handler.obtainMessage());
		} catch (Exception e) {
			handler.sendMessage(handler.obtainMessage());
			e.printStackTrace();
		}
	}
	protected String request(){
		InputStream is = null;
		HttpClient httpclient = null;
		//http post
		try{
				httpclient = getNewHttpClient();
		        HttpPost httppost = new HttpPost("http://mimosa.snu.ac.kr/~dusdk11/nfc/com_android.php");
		        httppost.setEntity(new UrlEncodedFormEntity(this.list, "utf8"));
		        HttpResponse response = httpclient.execute(httppost);
		        HttpEntity entity = response.getEntity();
		        is = entity.getContent();
		}catch(Exception e){
		        System.out.println("Error in http connection "+e.toString());
		        this.interrupt();
		}
		//convert response to string
		try{
		        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		        StringBuilder sb = new StringBuilder();
		        String line = null;
		        while ((line = reader.readLine()) != null) {
		                sb.append(line + "\n");
		        }
		        is.close();
		        return sb.toString();
		}catch(Exception e){
		        System.out.println("Error converting result "+e.toString());
		        return null;
		}
	}
	public HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore
                    .getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(
                    params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }
	public class MySSLSocketFactory extends SSLSocketFactory {
	    SSLContext sslContext = SSLContext.getInstance("TLS");

	    public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
	        super(truststore);

	        TrustManager tm = new X509TrustManager() {
	            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	            }

	            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	            }

	            public X509Certificate[] getAcceptedIssuers() {
	                return null;
	            }
	        };

	        sslContext.init(null, new TrustManager[] { tm }, null);
	    }

	    @Override
	    public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
	        return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
	    }

	    @Override
	    public Socket createSocket() throws IOException {
	        return sslContext.getSocketFactory().createSocket();
	    }
	}
	
}

