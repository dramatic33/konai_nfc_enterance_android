package com.example.nfc_enterance_android.DBModule;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;

abstract public class insertQuery extends query{
	final int SIZE = 1000000000;
	private long uNo;
	private String Name;
	private String Category;
	private String serialNumber;
	private int price;
	private String manager;
	private String sub_manager;
	private String part;
	private String purchase_date;
	public insertQuery(Activity Context){
		super(Context);
		this.Name = "NULL";
		this.price = 0;
		this.manager = "NULL";
		this.sub_manager = "NULL";
		this.part = "NULL";
	}
	public void setuName(String Name){
		this.Name=Name;
	}
	public void setmanager(String manager){
		this.manager=manager;
	}
	public void setSub_manager(String sub_manager){
		this.sub_manager=sub_manager;
	}
	public void setpart(String part){
		this.part=part;
	}
	public void setPrice(int Price){
		this.price=Price;
	}
	public void setCategory(String Category){
		this.Category=Category;
	}
	public void setSerialNumber(String serialNumber){
		this.serialNumber=serialNumber;
	}
	public void setPurchaseDate(String Date){
		this.purchase_date=Date;
	}
	public void setuNo(long uNo){
		this.uNo=uNo;
	}
	public String execution() throws JSONException{
		this.clear();
		
		post("uNo", uNo);
		post("log_iNo", 0);
		post("mode", "UINSERT");
		post("cate", Category);
		post("serial", serialNumber);
		post("price", price);
		post("uName", Name);
		post("manager", manager);
		post("sub_manager", sub_manager);
		post("part", part);
		post("purchase_date", purchase_date);
		
		JSONObject json = new JSONObject(this.request());
		return json.getString("result");
	}
}