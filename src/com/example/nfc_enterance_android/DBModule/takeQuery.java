package com.example.nfc_enterance_android.DBModule;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

abstract public class takeQuery extends query{
	private Activity Context;
	protected long uNo;
	protected int fNo;
	protected int mNo;
	protected Bitmap pictre;
	protected String readResult;
	public takeQuery(Activity Context, long uNo, int mNo){
		this.Context=Context;
		this.uNo=uNo;
		this.mNo=mNo;
		try {
			new recent_logQuery(uNo, mNo, Context){
				public void postprocess() {
					afterReadProcess();
				}
			}.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public abstract void setPicture(JSONObject json);
	public abstract void afterReadProcess();
	public void makeProgress(){
		this.makeDialog(Context);
	}
	public void setfNo(int fNo){
		this.fNo=fNo;
	}
	public void setmNo(int mNo){
		this.mNo=mNo;
	}
	public String getReadResult(){
		return this.readResult;
	}
	public Bitmap getPicture(){
		return this.pictre;
	}
	@Override
	public String execution() throws Exception {
		this.clear();
		
		post("mode", "TAKE_IN");
		post("unit_no", uNo);
		post("member_no", mNo);
		post("fNo", fNo);
		
		String result = this.request();
		JSONObject json = new JSONObject(result);
		return json.getString("result");
	}
	abstract private class recent_logQuery extends query{
		private recent_logQuery(long uNo, int mNo, Activity Context){
			super(Context);
			post("mode", "RECENT_LOG");
			post("mNo", mNo);
			post("uNo", String.valueOf(uNo));
		}
		public String execution() throws Exception {
			readResult = this.request();
			return readResult;
		}
		@Override
		public void getInformation() {
			// TODO Auto-generated method stub
			
		}
		public void run(){
			try {
				this.getInformation();
				this.result = this.execution();
				JSONObject json = new JSONObject(this.result);
				setPicture(json);
				handler.sendMessage(handler.obtainMessage());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	protected Bitmap getBitmapFromURL(String src) {
	      HttpURLConnection connection = null;
	      try {
	          URL url = new URL(src);
	          connection = (HttpURLConnection) url.openConnection();
	          connection.setDoInput(true);
	          connection.connect();
	          InputStream input = connection.getInputStream();
	          BitmapFactory.Options option = new BitmapFactory.Options();
	          option.inSampleSize = 2;
	          option.inPreferQualityOverSpeed = Bitmap.Config.ARGB_8888 != null;
	          Bitmap myBitmap = BitmapFactory.decodeStream(input, null, option);
	          if(connection!=null)connection.disconnect();
	          return myBitmap;
	      } catch (IOException e) {
	    	  e.printStackTrace();
	    	  if(connection!=null)connection.disconnect();
	          return null;
	      }
	}

}