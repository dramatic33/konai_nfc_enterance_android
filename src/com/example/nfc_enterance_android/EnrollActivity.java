package com.example.nfc_enterance_android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class EnrollActivity extends Activity {

	Button NewBtn;
	Button ModifyBtn;
	Button DeleteBtn;
	
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.reg_activity);
	    
	    NewBtn = (Button)findViewById(R.id.newRegBtn);
	    ModifyBtn = (Button)findViewById(R.id.editBtn);
	    DeleteBtn = (Button)findViewById(R.id.deleteBtn);
	    
	    NewBtn.setOnClickListener(butListener);
	    ModifyBtn.setOnClickListener(butListener);
	    DeleteBtn.setOnClickListener(butListener);
	    
	}
	
	private OnClickListener butListener = new OnClickListener(){
		public void onClick(View v) {
			Button clicked = (Button)v;
			Intent intent = null;
			if(clicked.equals(NewBtn)){
				intent = new Intent(EnrollActivity.this, Enroll_NewActivity.class);
			} else if(clicked.equals(ModifyBtn)){
				intent = new Intent(EnrollActivity.this, Enroll_ModifyActivity.class);
			} else if(clicked.equals(DeleteBtn)){
				intent = new Intent(EnrollActivity.this, Enroll_Delete.class);
			} else {
				//TODO Exception Handling
				return;
			}
			startActivity(intent);
		}
	};
}
