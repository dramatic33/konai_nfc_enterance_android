package com.example.nfc_enterance_android;

import com.example.nfc_enterance_android.DBModule.deleteQuery;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class Enroll_Delete extends Activity {

	final static int REQUEST_CODE = 1;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    //NFC Detect 엑티비티를 실행하여
	    Intent intent = new Intent(this, NFCDelActivity.class);
		this.startActivityForResult(intent, REQUEST_CODE);
	}
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		//되돌아오면
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode){
			case REQUEST_CODE:{
				if(resultCode==Activity.RESULT_OK){
					//nfc ID를 추출하여
					final int nfcID = data.getIntExtra("nfcID", 0);
					//deleteQuery를 실행
					deleteQuery del = new deleteQuery(this){
						public void getInformation() {
							this.setuNo(nfcID);
						}
						public void postprocess() {
							//결과를 Toast로 띄우고 끝낸다.
							Toast.makeText(Enroll_Delete.this, parseResultCode(this.getResult()), Toast.LENGTH_LONG).show();							
							Enroll_Delete.this.finish();
						}
					};
					del.start();
				}
				break;
			}
		}
	}
	private String parseResultCode(String ResultCodeString){
		int ResultCode = Integer.valueOf(ResultCodeString);
		String ResultMessage = null;
		switch(ResultCode){
			case 0: ResultMessage = getResources().getString(R.string.TEXT_ENROLL_DELETE_RESULT_SUCCESS); break;
			default: ResultMessage = getResources().getString(R.string.TEXT_RESULT_UNKNOWN); break;
		}
		
		return ResultMessage;
	}
}
