package com.example.nfc_enterance_android;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.nfc_enterance_android.DBModule.loginQuery;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {
	EditText idText;
	EditText passText;
	Button loginButton;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    this.setContentView(R.layout.log_in_activity);
	    
	    //컴포넌트 세팅
	    idText = (EditText)findViewById(R.id.login_id);
	    passText = (EditText)findViewById(R.id.login_pass);
	    loginButton = (Button)findViewById(R.id.login_button);
	    
	    loginButton.setOnClickListener(click);
	}
	public void onBackPressed(){
		//Back을 누르면 어플을 꺼버린다.
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
	private OnClickListener click = new OnClickListener(){
		public void onClick(View arg0) {
				//로그인 버튼을 누르면
				new loginQuery(LoginActivity.this){

					//정보를 받아
					public void getInformation() {
						this.setID(idText.getText().toString());
						this.setPassword(passText.getText().toString());
					}

					//로그인이 끝나면
					public void postprocess() {
						try{
							//mNo를 받아 음수면 오류메세지를 띄우고
							JSONObject json = new JSONObject(this.getResult());
							int mNo = json.getInt("mNo");
							String authCode = "0";
							String ResultMessage = null;
							switch(mNo){
								case -1:ResultMessage=getResources().getString(R.string.TEXT_LOGIN_RESULT_WRONG_PASSWORD); break;
								case -2:ResultMessage=getResources().getString(R.string.TEXT_LOGIN_RESULT_WRONG_ID); break;
								default:{
									ResultMessage=json.getString("name")+getResources().getString(R.string.TEXT_LOGIN_WELCOME);
									authCode = json.getString("authCode");
									break;
								}
							}	
							Toast.makeText(LoginActivity.this, ResultMessage, Toast.LENGTH_LONG).show();
							if(mNo>0){
								//로그인에 성공하면 mNo, authCode, ID, password를 넣어 메인 엑티비티로 보낸다
								Intent resultIntent = new Intent();
								resultIntent.putExtra("mNo", mNo);
								resultIntent.putExtra("authCode", authCode);
								resultIntent.putExtra("ID", idText.getText().toString());
								resultIntent.putExtra("password", passText.getText().toString());
								setResult(Activity.RESULT_OK, resultIntent);
								finish();
							}
						} catch (JSONException e){
							e.printStackTrace();
						}
						
					}
					
				}.start();
		}
	};

}
