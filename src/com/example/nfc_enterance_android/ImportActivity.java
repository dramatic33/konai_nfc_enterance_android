package com.example.nfc_enterance_android;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.nfc_enterance_android.DBModule.takeinQuery;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.Toast;

public class ImportActivity extends Activity{
	final private static int REQUEST_CODE = 1;
	
	private EditText unitName;
	private EditText memberName;
	private ImageView pictureView;
	private EditText exportFrom;
	private EditText ImportDate;
	private RadioGroup exit;
	private RadioGroup whose;
	private EditText reason;
	private Button submit;
	
	private long nfcID;
	
	private takeinQuery takeIn;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    this.setContentView(R.layout.export_request_activity);
	    
	    unitName = (EditText)findViewById(R.id.exportItemEntity);
	    memberName = (EditText)findViewById(R.id.exportMemberName);
	    pictureView = (ImageView)findViewById(R.id.exportPictureEntity);
	    exportFrom = (EditText)findViewById(R.id.exportFromEntry);
	    ImportDate = (EditText)findViewById(R.id.importDateEntry);
	    exit = (RadioGroup)findViewById(R.id.exitRadioGroup);
	    whose = (RadioGroup)findViewById(R.id.whoseRadioGroup);
	    reason = (EditText)findViewById(R.id.exportReasonEntry);
	    
	    TableRow butArea = (TableRow)findViewById(R.id.exportButtonArea);
		TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
		submit = new Button(ImportActivity.this);
		submit.setWidth(300);
		submit.setText(getResources().getString(R.string.TEXT_KEY_OK));
		submit.setOnClickListener(click);
		butArea.addView(submit, lp);
	    
	    exportFrom.setKeyListener(null);
	    ImportDate.setKeyListener(null);
	    reason.setKeyListener(null);
	    
	    for(int i=0;i<exit.getChildCount();i++){
	    	exit.getChildAt(i).setEnabled(false);
	    }
	    for(int i=0;i<whose.getChildCount();i++){
	    	whose.getChildAt(i).setEnabled(false);
	    }
	    
	    submit.setOnClickListener(click);
	    
	    Intent intent = new Intent(this, NFCDetectActivity.class);
	    this.startActivityForResult(intent, REQUEST_CODE);
	}
	private OnClickListener click = new OnClickListener(){
		@Override
		public void onClick(View v) {
			Button clicked = (Button)v;
			if(clicked.equals(submit)){
				submit();
			}
		}
		
	};
	private void submit(){
		takeIn.makeProgress();
		takeIn.start();
	}
	private String parseResultCode(String ResultCodeString){
		int ResultCode = Integer.valueOf(ResultCodeString);
		String ResultMessage = null;
		switch(ResultCode){
			case 0: ResultMessage = getResources().getString(R.string.TEXT_IMPORT_RESULT_SUCCESS); break;
			case -1: ResultMessage = getResources().getString(R.string.TEXT_IMPORT_RESULT_NOT_APPROVED); break;
			case -2: ResultMessage = getResources().getString(R.string.TEXT_IMPORT_RESULT_ALREADY); break;
			default: ResultMessage = getResources().getString(R.string.TEXT_RESULT_UNKNOWN); break;
		}
		
		return ResultMessage;
	}
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode){
			case REQUEST_CODE:{
				if(resultCode==Activity.RESULT_OK){
					nfcID = data.getLongExtra("nfcID", 0);
					takeIn = new takeinQuery(this, nfcID, MainActivity.user.getmNo()){
						public void getInformation() {
							takeIn.setmNo(MainActivity.user.getmNo());
						}
						public void postprocess() {
							Toast.makeText(ImportActivity.this, parseResultCode(this.getResult()), Toast.LENGTH_LONG).show();
							ImportActivity.this.finish();
						}
						public void afterReadProcess() {
							takeIn.setmNo(MainActivity.user.getmNo());
							try{
								JSONObject json = new JSONObject(this.getReadResult());
								takeIn.setfNo(Integer.valueOf(json.getString("fNo")));
								
								unitName.setText(json.getString("uName"));
								memberName.setText(json.getString("name"));
								Bitmap Image = getPicture();
								if(Image==null) Image = BitmapFactory.decodeResource(getResources(), R.drawable.unknown_person);
								pictureView.setImageBitmap(Image);
								
								int state = json.getInt("state");
								
								switch(state){
									case 0:
									case 1:{
										Toast.makeText(ImportActivity.this, getResources().getString(R.string.TEXT_IMPORT_RESULT_NOT_APPROVED), Toast.LENGTH_LONG).show();
										ImportActivity.this.finish();
										break;
									}
									case 2:{
										exportFrom.setText(json.getString("export_to"));
										ImportDate.setText(json.getString("import_date"));
										reason.setText(json.getString("reason"));
								    
										String exitValue = json.getString("exit");
										String whoseValue = json.getString("whose");
								    
										if(exitValue.compareTo(getResources().getString(R.string.TEXT_KEY_FORM_EXITVALUE_FRONT))==0) exit.check(R.id.exitRadio01);
										else if(exitValue.compareTo(getResources().getString(R.string.TEXT_KEY_FORM_EXITVALUE_EXIT))==0) exit.check(R.id.exitRadio02);
										else ;//TODO
								    
										if(whoseValue.compareTo(getResources().getString(R.string.TEXT_KEY_FORM_WHOSE_COMPANY))==0) whose.check(R.id.whoseRadio01);
										else if(whoseValue.compareTo(getResources().getString(R.string.TEXT_KEY_FORM_WHOSE_ABROAD))==0) whose.check(R.id.whoseRadio02);
										else if(whoseValue.compareTo(getResources().getString(R.string.TEXT_KEY_FORM_WHOSE_ETC))==0) whose.check(R.id.whoseRadio03);
										else ; //TODO
										break;
									}
								case 3:{
										Toast.makeText(ImportActivity.this, getResources().getString(R.string.TEXT_IMPORT_RESULT_ALREADY), Toast.LENGTH_LONG).show();
										ImportActivity.this.finish();
										break;
									}
								}
							} catch (JSONException e){
								e.printStackTrace();
							}
						}
					};
				}
				break;
			}
		}
	}

}
