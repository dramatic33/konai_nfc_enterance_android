package com.example.nfc_enterance_android;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.StringTokenizer;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.nfc_enterance_android.DBModule.LogDetailQuery;
import com.example.nfc_enterance_android.DBModule.logQuery;
import com.example.nfc_enterance_android.DBModule.searchQuery;

import android.text.TextUtils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class InquiryActivity extends Activity {
	final static int ClickTag = 1;
	searchQuery search;
	TableLayout table;
	ScrollView scroll;
	
	AlertDialog alert;
	int scroll_y = 690;
	int page = 1;
	int rowCount = 1;
	
	int mode = 0;
	String key = "";
	String value = "";
	boolean DatePickCalled = true;
	Bitmap Picture;
	
	TextView Takeheader;
	TextView Dateheader;
	TextView Unitheader;
	TextView Memberheader;
	
	ViewTreeObserver observer;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    this.setContentView(R.layout.inquiry_activity);
	    scroll = (ScrollView)findViewById(R.id.inquiry_scroll);
	    observer = scroll.getViewTreeObserver();
		observer.addOnScrollChangedListener(scrollListener);
		
		Takeheader = (TextView)findViewById(R.id.inquiry_header1);
		Takeheader.setOnClickListener(click);
		Dateheader = (TextView)findViewById(R.id.inquiry_header2);
		Dateheader.setOnClickListener(click);
		Unitheader = (TextView)findViewById(R.id.inquiry_header3);
		Unitheader.setOnClickListener(click);
		Memberheader = (TextView)findViewById(R.id.inquiry_header4);
		Memberheader.setOnClickListener(click);
		
	    table= (TableLayout)findViewById(R.id.inquiry_table);
	    try {
			this.addRow(1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	OnScrollChangedListener scrollListener = new OnScrollChangedListener(){
		public void onScrollChanged() {
			if(scroll_y<scroll.getScrollY()){
				scroll_y += 1650;
				page++;
				addRow(page);
			}
		}
	};
	private void addRow(final int page){
		logQuery log = new logQuery(this){
			public void getInformation() {
				this.setPage(page);	
				this.setKey(key);
				this.setValue(value);
			}
			public void postprocess() {
				StringTokenizer tokenizer = new StringTokenizer(this.getResult(), "\n");
				try{
					while(tokenizer.hasMoreTokens()){
						JSONArray json = new JSONArray(tokenizer.nextToken());
						addRow_internal(json);
						rowCount++;
					}
				}catch(JSONException e){
					e.printStackTrace();
				}
				
			}
		};
		log.start();
	}
	private void addRow_internal(JSONArray json) throws JSONException{
		String iNo = json.getString(5);
		
		TableRow row = new TableRow(this);
		row.setBackgroundResource(R.color.white);
		TableLayout.LayoutParams lp = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
		lp.setMargins(10, 0, 10, 10);
		row.setLayoutParams(lp);
		
		TableRow.LayoutParams trlp = new TableRow.LayoutParams();
		trlp.height = 100;
		trlp.leftMargin = 6;
		int textSize = 16;
		int textGrivity = Gravity.CENTER_VERTICAL;
		
		TextView tv = new TextView(this);
		tv.setBackgroundResource(R.color.white);
		tv.setTextSize(textSize);
		tv.setGravity(textGrivity);
		tv.setLines(1);
		tv.setEllipsize(TextUtils.TruncateAt.END);
		if(json.getString(0).compareTo("TAKE_IN")==0)	tv.setText(getResources().getString(R.string.TEXT_KEY_IMPORT));
		else if(json.getString(0).compareTo("TAKE_OUT")==0)tv.setText(getResources().getString(R.string.TEXT_KEY_EXPORT_REQUEST));
		else if(json.getString(0).compareTo("CONFIRM")==0) tv.setText(getResources().getString(R.string.TEXT_KEY_EXPORT_APPROVE));
		tv.setTag("take_form "+iNo);
		tv.setOnClickListener(click);
		row.addView(tv, trlp);
		
		tv = new TextView(this);
		tv.setTextSize(textSize);
		tv.setTag("date "+iNo);
		tv.setBackgroundResource(R.color.white);
		tv.setGravity(textGrivity);
		tv.setLines(1);
		tv.setEllipsize(TextUtils.TruncateAt.END);
		//tv.setOnClickListener(click);
		tv.setText(json.getString(1));
		row.addView(tv, trlp);
		
		tv = new TextView(this);
		tv.setTag("unit "+iNo);
		tv.setTextSize(textSize);
		tv.setBackgroundResource(R.color.white);
		tv.setLines(1);
		tv.setEllipsize(TextUtils.TruncateAt.END);
		tv.setGravity(textGrivity);
		tv.setText(json.getString(2));
		tv.setOnClickListener(click);
		row.addView(tv, trlp);
		
		tv = new TextView(this);
		tv.setTextSize(textSize);
		tv.setTag("member "+iNo);
		tv.setBackgroundResource(R.color.white);
		tv.setLines(1);
		tv.setEllipsize(TextUtils.TruncateAt.END);
		tv.setGravity(textGrivity);
		tv.setText(json.getString(3));
		tv.setOnClickListener(click);
		row.addView(tv, trlp);

		table.addView(row);
	}
	private void clearTable(){
		for(int i=(rowCount-1);i>0;i--){
			table.removeViewAt(i);
		}
		this.page=1;
		this.rowCount=1;
		this.scroll_y = 690;
	}
	private OnClickListener click = new OnClickListener(){
		public void onClick(View arg0) {
			TextView clicked = (TextView)arg0;
			if(clicked.equals(Takeheader)){
				final CharSequence[] List = {getResources().getString(R.string.TEXT_KEY_ALL), getResources().getString(R.string.TEXT_KEY_EXPORT_WAIT),
						getResources().getString(R.string.TEXT_KEY_NOT_IMPORTED), getResources().getString(R.string.TEXT_KEY_IMPORT),
						getResources().getString(R.string.TEXT_KEY_EXPORT_REQUEST), getResources().getString(R.string.TEXT_KEY_EXPORT_APPROVE)};
				AlertDialog.Builder alt_bld = new AlertDialog.Builder(InquiryActivity.this);
		        alt_bld.setTitle("보고자 하는 항목을 선택하세요");
		        alt_bld.setSingleChoiceItems(List, -1, new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int item) {
		            	key = "flag";
						switch(item){
							case 0:value = ""; Takeheader.setText(getResources().getString(R.string.TEXT_KEY_ALL)); break;
							case 1: key = "state"; value=Integer.toString(0); Takeheader.setText(getResources().getString(R.string.TEXT_KEY_EXPORT_WAIT)); break;
							case 2: key = "state"; value=Integer.toString(2); Takeheader.setText(getResources().getString(R.string.TEXT_KEY_NOT_IMPORTED)); break;
							case 3:value = "TAKE_IN"; Takeheader.setText(getResources().getString(R.string.TEXT_KEY_IMPORT)); break;
							case 4:value = "TAKE_OUT"; Takeheader.setText(getResources().getString(R.string.TEXT_KEY_EXPORT_REQUEST)); break;
							case 5:value = "CONFIRM"; Takeheader.setText(getResources().getString(R.string.TEXT_KEY_EXPORT_APPROVE)); break;
						}
						clearTable();
						addRow(1);
						alert.dismiss();
		            }
		        });
		        alert = alt_bld.create();
		        alert.show();
			} else if(clicked.equals(Dateheader)){
				Calendar c = Calendar.getInstance();
				int cyear = c.get(Calendar.YEAR);
				int cmonth = c.get(Calendar.MONTH);
				int cday = c.get(Calendar.DAY_OF_MONTH);
				     
				DatePickerDialog alert = new DatePickerDialog(InquiryActivity.this, new DatePickerDialog.OnDateSetListener() {
					@Override
				 	public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						key="date";
						value=year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
						DatePickCalled = !DatePickCalled;
						if(!DatePickCalled){clearTable(); addRow(1);}
				  	}
				}, cyear, cmonth, cday);
				alert.show();
			} else if(clicked.equals(Unitheader)){
				AlertDialog.Builder builder = new AlertDialog.Builder(InquiryActivity.this);
				final EditText input = new EditText(InquiryActivity.this);
				builder.setMessage(getResources().getString(R.string.TEXT_INQUIRY_COMMAND_ENTER_SEARCH));
				builder.setView(input);
				
				builder.setPositiveButton(getResources().getString(R.string.TEXT_KEY_OK), new DialogInterface.OnClickListener(){
					public void onClick(DialogInterface dialog, int which) {
						key="uName";
						value = input.getText().toString();
						clearTable();
						addRow(1);
					}
				}).show();
			} else if(clicked.equals(Memberheader)){
				AlertDialog.Builder builder = new AlertDialog.Builder(InquiryActivity.this);
				final EditText input = new EditText(InquiryActivity.this);
				builder.setMessage(getResources().getString(R.string.TEXT_INQUIRY_COMMAND_ENTER_SEARCH));
				builder.setView(input);
				
				builder.setPositiveButton(getResources().getString(R.string.TEXT_KEY_OK), new DialogInterface.OnClickListener(){
					public void onClick(DialogInterface dialog, int which) {
						key="mName";
						value = input.getText().toString();
						clearTable();
						addRow(1);
					}
				}).show();
			} else {
				String ID = (String) arg0.getTag();
				StringTokenizer tokenizer = new StringTokenizer(ID, " ");
				final String type = tokenizer.nextToken();
				final String iNo = tokenizer.nextToken();
				
				new LogDetailQuery(InquiryActivity.this){
					public void getInformation() {
						this.setType(type);
						this.setiNo(iNo);
					}
					public void postprocess() {
						try{
							ArrayList<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
							String result = this.getResult();
							if(result.compareTo("false\n")==0){
								Toast.makeText(InquiryActivity.this, getResources().getString(R.string.TEXT_INQUIRY_DELETED), Toast.LENGTH_LONG).show();
							} else{
								JSONObject json = new JSONObject(result);
								if(type.compareTo("unit")==0){
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_UNIT_NAME),json.getString("uName")));
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_UNIT_PRICE),json.getString("price")));
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_UNIT_MANAGER)+getResources().getString(R.string.TEXT_KEY_UNIT_MANAGER_PRI),json.getString("manager")));
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_UNIT_MANAGER)+getResources().getString(R.string.TEXT_KEY_UNIT_MANAGER_SUB),json.getString("sub_manager")));
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_UNIT_PART),json.getString("part")));
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_UNIT_CATEGORY),json.getString("category")));
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_UNIT_NUMBER),json.getString("serialNumber")));
								}else if(type.compareTo("member")==0){
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_MEMBER_NAME),json.getString("name")));
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_MEMBER_NUMBER),json.getString("pNo")));
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_MEMBER_PART),json.getString("part")));
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_MEMBER_PHONE),json.getString("phone")));
									Picture = this.getPicrue();
								}else if(type.compareTo("date")==0){
									
								}else if(type.compareTo("take_form")==0){
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_TAKEFORM),getResources().getString(R.string.TEXT_KEY_TAKEFORM)));
									if(json.getInt("state")==0) list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_FORM_STATE),getResources().getString(R.string.TEXT_KEY_EXPORT_WAIT)));
									else if(json.getInt("state")==2) list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_FORM_STATE),getResources().getString(R.string.TEXT_KEY_NOT_IMPORTED)));
									else list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_FORM_STATE),getResources().getString(R.string.TEXT_KEY_IMPORTED)));
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_FORM_TO),json.getString("export_to")));
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_FORM_IMPORTDATE),json.getString("import_date")));
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_FORM_EXITVALUE),json.getString("exit")));
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_FORM_WHOSE),json.getString("whose")));
									list.add(new BasicNameValuePair(getResources().getString(R.string.TEXT_KEY_FORM_REASON),json.getString("reason")));
								}
								new ShowInfoDialog(InquiryActivity.this, list).show();
							}
						} catch(JSONException e){
							e.printStackTrace();
						}
					}
					
				}.start();
			}
		}
	};
	private class ShowInfoDialog extends Dialog implements View.OnClickListener{
		private ShowInfoDialog(Context context, ArrayList<BasicNameValuePair> list){
			super(context);
			this.setTitle(list.get(0).getValue()+" "+getResources().getString(R.string.TEXT_INQUIRY_DETAIL));
			WindowManager.LayoutParams params = this.getWindow().getAttributes();
			params.width = WindowManager.LayoutParams.MATCH_PARENT;
			params.height = 1200;
			this.getWindow().setAttributes(params);
			
			LinearLayout container = new LinearLayout(context);
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			TableLayout table = new TableLayout(context);
			
			LinearLayout.LayoutParams tb = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			TableLayout.LayoutParams llp = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
			for(int i=0;i<list.size();i++){
				if(i==0&&list.get(0).getName().compareTo(getResources().getString(R.string.TEXT_KEY_TAKEFORM))==0) continue; 
				TableRow row = new TableRow(context);
				row.setBackgroundResource(R.color.bright_gray);
				TableRow.LayoutParams tvlp = new TableRow.LayoutParams();
				tvlp.setMargins(0, 0, 0, 4);
				tvlp.height=70;
				TextView tv = new TextView(context);
				tv.setText(list.get(i).getName());
				tv.setTextSize(15);
				tv.setWidth(250);
				//tv.setLines(1);
				tv.setEllipsize(TextUtils.TruncateAt.END);
				tv.setGravity(Gravity.CENTER);
				tv.setBackgroundResource(R.color.white);
				row.addView(tv, tvlp);
				
				tv = new TextView(context);
				tv.setText(list.get(i).getValue());
				tv.setTextSize(15);
				//tv.setLines(1);
				tv.setEllipsize(TextUtils.TruncateAt.END);
				tv.setWidth(350);
				tv.setGravity(Gravity.CENTER);
				tv.setBackgroundResource(R.color.white);
				row.addView(tv, tvlp);
				
				table.addView(row, llp);				
			}
			if(list.get(0).getName().compareTo(getResources().getString(R.string.TEXT_KEY_MEMBER_NAME))==0){
				TableRow row  = new TableRow(context);
				row.setBackgroundResource(R.color.bright_gray);
				TableRow.LayoutParams tvlp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
				tvlp.setMargins(0, 0, 0, 4);
				
				TextView tv = new TextView(context);
				tv.setText(getResources().getString(R.string.TEXT_KEY_MEMBER_PICTURE));
				tv.setTextSize(15);
				tv.setWidth(250);
				tv.setHeight(400);
				tv.setGravity(Gravity.CENTER);
				tv.setBackgroundResource(R.color.white);
				row.addView(tv, tvlp);
				
				ImageView iv = new ImageView(context);
				tvlp.height = 400;
				tvlp.width = 300;
				if(Picture!=null) iv.setImageBitmap(Picture);
				else iv.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.unknown_person));
				iv.setMaxHeight(400);
				iv.setMaxWidth(300);
				row.addView(iv, tvlp);
				
				table.addView(row, llp);	
			}
			container.addView(table, tb);
			Button button = new Button(context);
			button.setText(getResources().getString(R.string.TEXT_KEY_OK));
			button.setOnClickListener(this);
			table.addView(button, llp);
			this.addContentView(container, lp);
		}

		@Override
		public void onClick(View v) {
			this.dismiss();			
		}
	}
	
}
