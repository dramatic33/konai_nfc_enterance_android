package com.example.nfc_enterance_android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class NFCDetectActivity extends NFCProcess {

	private EditText UnitID;
	private Button Search;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    this.setContentView(R.layout.nfc_read_activity);
	    UnitID = (EditText)findViewById(R.id.serialNumberEntry);
	    Search = (Button)findViewById(R.id.okBtn);
	    Search.setOnClickListener(click);
	}
	OnClickListener click = new OnClickListener(){
		public void onClick(View arg0) {
			Button clicked = (Button)arg0;
			if(clicked.equals(Search)){
				Intent resultIntent = new Intent();
				String result = UnitID.getText().toString();
				resultIntent.putExtra("fromTag", false);
				resultIntent.putExtra("serialNumber", result);
				NFCDetectActivity.this.setResult(Activity.RESULT_OK, resultIntent);
				NFCDetectActivity.this.finish();
			} else {
				//TODO Exception Handling
			}
		}
	};
	public void onBackPressed(){
		  Intent intent = new Intent(this,MainActivity.class);
		  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		  startActivity(intent);
	}
	@Override
	void returnTo() {
		Intent resultIntent = new Intent();
		resultIntent.putExtra("fromTag", true);
		resultIntent.putExtra("nfcID", this.nfcID);
		this.setResult(Activity.RESULT_OK, resultIntent);
	}

}
