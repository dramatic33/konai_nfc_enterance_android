package com.example.nfc_enterance_android;

import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.nfc_enterance_android.DBModule.insertQuery;
import com.example.nfc_enterance_android.DBModule.modifyQuery;
import com.example.nfc_enterance_android.DBModule.searchQuery;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

public class Enroll_ModifyActivity extends Activity {

	int nfcID;
	int newnfcID;
	boolean DatePickerEnable = true;
	
	EditText SerialText;
	EditText NameText;
	EditText PurchaseDateText;
	EditText PriceText;
	EditText Manger1DText;
	EditText Manger1Text;
	EditText SectionText;
	EditText ClassText;
	Button SubmitBtn;
	
	final static int DETECT_REQEUST_CODE = 1;
	
	insertQuery insert;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    this.setContentView(R.layout.reg_new_activity);
	    
	    ((EditText)findViewById(R.id.newRegTitleLabel)).setText(getResources().getString(R.string.TEXT_TITLE_ENROLL_MODIFY));
	    
	    SerialText=(EditText)findViewById(R.id.serialNumberEntry);
		NameText=(EditText)findViewById(R.id.nameEntry);
		PurchaseDateText=(EditText)findViewById(R.id.purchaseDateEntry);
		PriceText=(EditText)findViewById(R.id.priceEntry);
		Manger1DText=(EditText)findViewById(R.id.mainManagerEntry);
		Manger1Text=(EditText)findViewById(R.id.viceManagerEntry);
		SectionText=(EditText)findViewById(R.id.departmentEntry);
		ClassText=(EditText)findViewById(R.id.categoryEntry);
		
		SubmitBtn=(Button)findViewById(R.id.newRegOkButton);
		
		PurchaseDateText.setOnTouchListener(touch);
		
		SubmitBtn.setOnClickListener(butListener);
		
		//get ID
		Intent intent = new Intent(this, NFCDetectActivity.class);
		this.startActivityForResult(intent, DETECT_REQEUST_CODE);
	}
	
	private OnTouchListener touch = new OnTouchListener(){
		public boolean onTouch(View arg0, MotionEvent arg1) {
			EditText touched = (EditText)arg0;
			if(touched.equals(PurchaseDateText)&&DatePickerEnable){
				DatePickerEnable=false;
				
				Calendar c = Calendar.getInstance();
				int cyear = c.get(Calendar.YEAR);
				int cmonth = c.get(Calendar.MONTH);
				int cday = c.get(Calendar.DAY_OF_MONTH);
				
				final DatePickerDialog alert = new DatePickerDialog(Enroll_ModifyActivity.this, null, cyear, cmonth, cday);
				alert.setCancelable(true);
				alert.setCanceledOnTouchOutside(true);
				alert.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.TEXT_KEY_OK),
						new DialogInterface.OnClickListener(){
							public void onClick(DialogInterface dialog, int which) {
								DatePicker picker = alert.getDatePicker();
								String Date =picker.getYear()+"-"+(picker.getMonth()+1)+"-"+picker.getDayOfMonth();
								PurchaseDateText.setText(Date);
								DatePickerEnable = true;
							}});
				alert.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.TEXT_KEY_CANCLE),
						new DialogInterface.OnClickListener(){
							public void onClick(DialogInterface dialog, int which) {
								alert.dismiss();
						
					}});
				alert.show();
			}
			return true;
		}
	};
	
	private OnClickListener butListener = new OnClickListener(){
		public void onClick(View v) {
			Button clicked = (Button)v;
			if(clicked.equals(SubmitBtn)){
				submit();
			} else {
				//TODO Exception Handling
				return;
			}
		}
	};
	private String parseResultCode(String ResultCodeString){
		int ResultCode = Integer.valueOf(ResultCodeString);
		String ResultMessage = null;
		switch(ResultCode){
			case 0: ResultMessage = getResources().getString(R.string.TEXT_ENROLL_DELETE_RESULT_SUCCESS); break;
			default: ResultMessage = getResources().getString(R.string.TEXT_RESULT_UNKNOWN); break;
		}
		
		return ResultMessage;
	}
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode){
			case DETECT_REQEUST_CODE:{
				if(resultCode==Activity.RESULT_OK){
					nfcID = data.getIntExtra("nfcID", 0);
					newnfcID = nfcID;
					updateScreen();
				}
				break;
			}
		}
	}
	private void updateScreen(){
		searchQuery search = new searchQuery(this){
			public void getInformation() {
				this.setTable(searchQuery.table.unit);
				this.setUnitKey(searchQuery.unit_key.uNo);
				this.setValue(Integer.toString(nfcID));
			}

			@Override
			public void postprocess() {
				try {
					String result = this.getResult();
					JSONObject json = new JSONObject(result);
					
					SerialText.setText(json.getString("serialNumber"));
					NameText.setText(json.getString("uName"));
					PurchaseDateText.setText(json.getString("purchase_date"));
					PriceText.setText(json.getString("price"));
					Manger1DText.setText(json.getString("manager"));
					Manger1Text.setText(json.getString("sub_manager"));
					SectionText.setText(json.getString("part"));
					ClassText.setText(json.getString("category"));
					
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		};
		search.start();
	}
	private void submit(){
		//if(CheckInput()){
			
			modifyQuery modify = new modifyQuery(this){

				@Override
				public void getInformation() {
					String Name = NameText.getText().toString();
					String Serial =SerialText.getText().toString();
					String PurchaseDate = PurchaseDateText.getText().toString();
					int Price = Integer.valueOf(PriceText.getText().toString());
					String Manger1D = Manger1DText.getText().toString();
					String Manger1 = Manger1Text.getText().toString();
					String Section = SectionText.getText().toString();
					String Class = ClassText.getText().toString();
					
					this.setuNo(nfcID);
					this.setnewNFCID(newnfcID);
					this.setuName(Name);
					this.setSerialNumber(Serial);
					this.setPurchaseDate(PurchaseDate);
					this.setPrice(Price);
					this.setmanager(Manger1D);
					this.setSub_manager(Manger1);
					this.setpart(Section);
					this.setCategory(Class);
					
				}

				@Override
				public void postprocess() {
					AlertDialog.Builder builder = new AlertDialog.Builder(Enroll_ModifyActivity.this);
					builder.setMessage(parseResultCode(this.getResult())).setPositiveButton(getResources().getString(R.string.TEXT_KEY_OK), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Enroll_ModifyActivity.this.finish();					
						}
					});
					builder.create().show();
				}
				
			};
			modify.start();
		//} else {
			
		//}
	}

}
