package com.example.nfc_enterance_android;

import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.nfc_enterance_android.DBModule.removeQuery;
import com.example.nfc_enterance_android.DBModule.searchQuery;
import com.example.nfc_enterance_android.DBModule.takeoutQuery;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TableRow.LayoutParams;

public class ExportActivity extends Activity{
	//Constant
	final private static int REQUEST_CODE = 1;

	//Query Value
	private long uNo;
	private int state; //state[0:반출요청 살태, 1:반출허가 상태, 2:반출 완료 상태 3:반입 상태]
	private String exitValue;
	private String whoseValue;
	
	//Query
	private takeoutQuery takeout;
	
	//Interface Component
	private EditText unit;
	private EditText member;
	private ImageView picture;
	private EditText exportFrom;
	private EditText ImportDate;
	private RadioGroup exit;
	private RadioGroup whose;
	private EditText reason;
	private Button submit;
	private Button cancle;
	
	//Control Value
	private boolean DatePickerEnable = true;
	private boolean modicatable = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    this.setContentView(R.layout.export_request_activity);
	    
	    //Set Interface Component
	    unit = (EditText)findViewById(R.id.exportItemEntity);
	    member = (EditText)findViewById(R.id.exportMemberName);
	    picture = (ImageView)findViewById(R.id.exportPictureEntity);
	    exportFrom = (EditText)findViewById(R.id.exportFromEntry);
	    ImportDate = (EditText)findViewById(R.id.importDateEntry);
	    exit = (RadioGroup)findViewById(R.id.exitRadioGroup);
	    whose = (RadioGroup)findViewById(R.id.whoseRadioGroup);
	    reason = (EditText)findViewById(R.id.exportReasonEntry);
	    
	    //Set default Value
	    exitValue = getResources().getString(R.string.TEXT_KEY_FORM_EXITVALUE_FRONT);
	    whoseValue = getResources().getString(R.string.TEXT_KEY_FORM_WHOSE_COMPANY);
	    exit.check(R.id.exitRadio01);
	    whose.check(R.id.whoseRadio01);
	    
	    //Set Listener
	    ImportDate.setOnTouchListener(touch);
	    exit.setOnCheckedChangeListener(check);
	    whose.setOnCheckedChangeListener(check);
	    
	    //Start NFC Read
	    Intent intent = new Intent(ExportActivity.this, NFCDetectActivity.class);
		this.startActivityForResult(intent, REQUEST_CODE);
	}
	
	//When Touch Date picker(Import Date)
	private OnTouchListener touch = new OnTouchListener(){
		public boolean onTouch(View arg0, MotionEvent arg1) {
			EditText touched = (EditText)arg0;
			if(touched.equals(ImportDate)&&DatePickerEnable){
				//LOCK
				DatePickerEnable=false;
				
				//Get Current Date;
				Calendar c = Calendar.getInstance();
				int cyear = c.get(Calendar.YEAR);
				int cmonth = c.get(Calendar.MONTH);
				int cday = c.get(Calendar.DAY_OF_MONTH);
				
				//Make DatePicker Dialog
				final DatePickerDialog alert = new DatePickerDialog(ExportActivity.this, null, cyear, cmonth, cday);
				alert.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.TEXT_KEY_OK),
						new DialogInterface.OnClickListener(){
							public void onClick(DialogInterface dialog, int which) {
								//Set Date
								DatePicker picker = alert.getDatePicker();
								String Date =picker.getYear()+"-"+(picker.getMonth()+1)+"-"+picker.getDayOfMonth();
								ImportDate.setText(Date);
								DatePickerEnable = true;
							}});
				alert.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.TEXT_KEY_CANCLE),
						new DialogInterface.OnClickListener(){
							public void onClick(DialogInterface dialog, int which) {
								//Dismiss Picker
								DatePickerEnable = true;
								alert.dismiss();
						
					}});
				alert.show();
			}
			return true;
		}
	};
	private OnClickListener click = new OnClickListener(){
		@Override
		public void onClick(View v) {
			Button clicked = (Button)v;
			//Click Submit
			if(clicked.equals(submit)) submit();
			else if(clicked.equals(cancle)) cancle();
		}
		
	};
	private void submit(){
		switch(state){
		//When ready to APPROVE (Modify or Approve)
			case 0: {
				AlertDialog.Builder builder = new AlertDialog.Builder(ExportActivity.this);
				StringBuilder sb = new StringBuilder();
				if(modicatable) sb.append(getResources().getString(R.string.TEXT_KEY_MODIFY)+"/");
				if(sb.length()==0){
					Toast.makeText(ExportActivity.this, getResources().getString(R.string.TEXT_AUTHORITY_NO), Toast.LENGTH_LONG);
					ExportActivity.this.finish();
				}
				sb.insert(0, getString(R.string.TEXT_KEY_TAKEFORM)+"을 ");
				sb.delete(sb.length()-1, sb.length());
				sb.append(" 하시겠습니까?");
				String Message = sb.toString();
				builder.setMessage(Message);
				
				//Make Dialog with Auth level and Owner of form
				if(modicatable) builder.setPositiveButton(getResources().getString(R.string.TEXT_KEY_MODIFY), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						takeout.setMode(1);
						takeout.start();
					}
				});
				builder.create().show();
				break;
			}
			case 1: {
				AlertDialog.Builder builder = new AlertDialog.Builder(ExportActivity.this);
				StringBuilder sb = new StringBuilder();
				
				//Make String with Auth level and Owner of form
				if(MainActivity.user.canApproveExport()) sb.append(getResources().getString(R.string.TEXT_KEY_APPROVE)+"/");
				
				if(sb.length()==0){
					Toast.makeText(ExportActivity.this, getResources().getString(R.string.TEXT_AUTHORITY_NO), Toast.LENGTH_LONG);
					ExportActivity.this.finish();
				}
				sb.insert(0, getString(R.string.TEXT_KEY_TAKEFORM)+"을 ");
				sb.delete(sb.length()-1, sb.length());
				sb.append(" 하시겠습니까?");
				String Message = sb.toString();
				builder.setMessage(Message);
				
				//Make Dialog with Auth level and Owner of form
				if(MainActivity.user.canApproveExport()) builder.setNegativeButton(getResources().getString(R.string.TEXT_KEY_APPROVE), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						takeout.setMode(2);
						takeout.start();
						ExportActivity.this.finish();					
					}
				});
				builder.create().show();
				break;
			}
			//When APPROVED : Error Message
			case 2: {
				Toast.makeText(ExportActivity.this, getResources().getString(R.string.TEXT_EXPORT_RESULT_ALREADY), Toast.LENGTH_LONG).show();
				ExportActivity.this.finish();
			}
			//When Imported : New Request to export
			case 3: {
				takeout.setMode(0);
				takeout.start();
				break;
			}
		}
	}
	private void cancle(){
		new removeQuery(this){
			@Override
			public void getInformation() {
				this.setType("take_form");
				this.setValue(Integer.toString(takeout.getfNo()));
			}
			@Override
			public void postprocess() {
				if(this.getResult().compareTo("0")==0){
					Toast.makeText(ExportActivity.this, getResources().getString(R.string.TEXT_EXPORT_APPROVE_RESULT_SUCCESS), Toast.LENGTH_LONG).show();
					ExportActivity.this.finish();
				} else {
					Toast.makeText(ExportActivity.this, getResources().getString(R.string.TEXT_EXORRT_CANCLE_RESULT_FAIL), Toast.LENGTH_LONG).show();
					ExportActivity.this.finish();
				}
			}
			
		}.start();
	}
	//Set Radio Button Event;
	private OnCheckedChangeListener check = new OnCheckedChangeListener(){
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			if(group.equals(exit)){
				switch(checkedId){
				case R.id.exitRadio01: exitValue=getResources().getString(R.string.TEXT_KEY_FORM_EXITVALUE_FRONT); break;
				case R.id.exitRadio02: exitValue=getResources().getString(R.string.TEXT_KEY_FORM_EXITVALUE_EXIT); break;
				default: //TODO Exception
				}
			} else if(group.equals(whose)){
				switch(checkedId){
				case R.id.whoseRadio01: whoseValue = getResources().getString(R.string.TEXT_KEY_FORM_WHOSE_COMPANY); break;
				case R.id.whoseRadio02: whoseValue = getResources().getString(R.string.TEXT_KEY_FORM_WHOSE_ABROAD); break;
				case R.id.whoseRadio03: whoseValue = getResources().getString(R.string.TEXT_KEY_FORM_WHOSE_ETC); break;
				default: //TODO Exception
				}
			} else {
				//TODO Exception
			}
		}
	};
	//Set End query Message
	private String parseResultCode(String ResultCodeString){
		int ResultCode = Integer.valueOf(ResultCodeString);
		String ResultMessage = null;
		switch(ResultCode){
			case 0:{
				switch(takeout.getMode()){
					case 0: ResultMessage = getResources().getString(R.string.TEXT_EXPORT_REQUEST_RESULT_SUCCESS); break;
					case 1: ResultMessage = getResources().getString(R.string.TEXT_EXPORT_MODIFY_RESULT_SUCCESS); break;
					case 2: ResultMessage = getResources().getString(R.string.TEXT_EXPORT_APPROVE_RESULT_SUCCESS); break;
				}
				break;
			}
			default: ResultMessage = getResources().getString(R.string.TEXT_RESULT_UNKNOWN); break;
		}
		
		return ResultMessage;
	}
	//Set After reading nfc tag -> Get DB result
	public void showInfo(JSONObject json) throws JSONException{
		exportFrom.setText(json.getString("export_to"));
	    ImportDate.setText(json.getString("import_date"));
	    reason.setText(json.getString("reason"));
	    
	    String exitValue = json.getString("exit");
	    String whoseValue = json.getString("whose");
	    
	    if(exitValue.compareTo(getResources().getString(R.string.TEXT_KEY_FORM_EXITVALUE_FRONT))==0) exit.check(R.id.exitRadio01);
	    else if(exitValue.compareTo(getResources().getString(R.string.TEXT_KEY_FORM_EXITVALUE_EXIT))==0) exit.check(R.id.exitRadio02);
	    else ;//TODO
	    
	    if(whoseValue.compareTo(getResources().getString(R.string.TEXT_KEY_FORM_WHOSE_COMPANY))==0) whose.check(R.id.whoseRadio01);
	    else if(whoseValue.compareTo(getResources().getString(R.string.TEXT_KEY_FORM_WHOSE_ABROAD))==0) whose.check(R.id.whoseRadio02);
	    else if(whoseValue.compareTo(getResources().getString(R.string.TEXT_KEY_FORM_WHOSE_ETC))==0) whose.check(R.id.whoseRadio03);
	}
	public void setTakeOutQuery(){
		//Make Takeout Query with unit number
		takeout = new takeoutQuery(ExportActivity.this, uNo, MainActivity.user.getmNo()){

			@Override
			public void afterReadProcess() {
				//Afer get previous take_form information
				try{
					JSONObject json = new JSONObject(this.getReadResult());
					
					//Set previous take_form Information(It must be first)
					unit.setText(json.getString("uName"));
					member.setText(json.getString("name"));
					Bitmap Image = getPicture();
					if(Image==null) Image = BitmapFactory.decodeResource(getResources(), R.drawable.unknown_person);
					picture.setImageBitmap(Image);
					
					//If this is requested People -> can modify form
					if(MainActivity.user.getmNo()==json.getInt("mNo")) modicatable=true;
					
					state = json.getInt("state");
					switch(state){
						//Befor approve can modify or approve
						case 0:
						case 1:	showInfo(json);	this.setfNo(json.getInt("fNo")); break;
						//After approve nothing can be done
						case 2:	Toast.makeText(ExportActivity.this, getResources().getString(R.string.TEXT_EXPORT_RESULT_ALREADY), Toast.LENGTH_LONG).show(); ExportActivity.this.finish(); break;
						default:	break;
					}
					TableRow butArea = (TableRow)findViewById(R.id.exportButtonArea);
					TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
					lp.width = 300;
					lp.height = 80;
					lp.leftMargin = 10;
					lp.rightMargin = 10;
					if(modicatable){
						//If it is not new activity -> cancallable
						cancle = new Button(ExportActivity.this);
						cancle.setText(getResources().getString(R.string.TEXT_EXPORT_CANCLE));
						cancle.setOnClickListener(click);
						cancle.setBackgroundResource(R.color.red);
						cancle.setTextColor(Color.WHITE);
						butArea.addView(cancle, lp);									
					}
					submit = new Button(ExportActivity.this);
					submit.setWidth(200);
					submit.setText(getResources().getString(R.string.TEXT_KEY_OK));
					submit.setOnClickListener(click);
					submit.setBackgroundResource(R.color.red);
					submit.setTextColor(Color.WHITE);
					butArea.addView(submit, lp);
					
				}catch(JSONException e){
					//First submit
					state = 3;
					TableRow butArea = (TableRow)findViewById(R.id.exportButtonArea);
					TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
					submit = new Button(ExportActivity.this);
					submit.setWidth(300);
					submit.setText(getResources().getString(R.string.TEXT_KEY_OK));
					submit.setOnClickListener(click);
					butArea.addView(submit, lp);
				}
				
			}

			@Override
			public void getInformation() {
				this.setmNo(MainActivity.user.getmNo());
                this.setExport_to(exportFrom.getText().toString());
                this.setImport_date(ImportDate.getText().toString());
                this.setExit(exitValue);
                this.setWhose(whoseValue);
                this.setReason(reason.getText().toString());
			}

			@Override
			public void postprocess() {
				Toast.makeText(ExportActivity.this, parseResultCode(this.getResult()), Toast.LENGTH_LONG).show();
				ExportActivity.this.finish();
				
			}
			
		};
	}
	//After nfc read
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode){
			case REQUEST_CODE:{
				if(resultCode==Activity.RESULT_OK){
					boolean fromTag = data.getBooleanExtra("fromTag", true);
					//READ unit Number
					if(fromTag){
						uNo = data.getLongExtra("nfcID", 0);
						setTakeOutQuery();
					}
					else{
						final String serialNumber = data.getCharSequenceExtra("serialNumber").toString();
						new searchQuery(ExportActivity.this){
							@Override
							public void getInformation() {
								this.setTable(searchQuery.table.unit);
								this.setUnitKey(searchQuery.unit_key.serialNumber);
								this.setValue(serialNumber);
							}
							@Override
							public void postprocess() {
								try{
									JSONObject json = new JSONObject(this.getResult());
									uNo = json.getLong("uNo");
									setTakeOutQuery();
								} catch (JSONException e){
									e.printStackTrace();
								}
							}
							
						}.start();
					}
				}	
				break;
			}
		}
	}

}
