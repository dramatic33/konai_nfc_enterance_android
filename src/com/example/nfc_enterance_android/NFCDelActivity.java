package com.example.nfc_enterance_android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class NFCDelActivity extends NFCProcess {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    this.setContentView(R.layout.nfc_write_activity);
	    // TODO Auto-generated method stub
	}

	@Override
	void returnTo() {
		Intent resultIntent = new Intent();
		resultIntent.putExtra("nfcID", this.nfcID);
		this.setResult(Activity.RESULT_OK, resultIntent);
	}
}
