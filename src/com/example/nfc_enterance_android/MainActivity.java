package com.example.nfc_enterance_android;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.nfc_enterance_android.DBModule.loginQuery;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

public class MainActivity extends Activity {
	//유저 정보
	public static UserInfo user;
	
	//로그인 엑티비티 후 REQUEST CODE
	final public static int REQUEST_CODE = 1;
	
	//UI 컴포넌트
	private Button EnrollBtn;
	private Button Inquiry;
	private Button Export;
	private Button Import;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//유저가 없다면 로그인
		if(user==null){
			//SharedPreferences에서 ID와 비밀번호를 꺼낸다.
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
			final String ID = prefs.getString("ID", "");
			final String pass = prefs.getString("Pass", "");
			
			//저장된 정보가 있다면 이것으로 로그인을 실시한다.
			if(ID.compareTo("")!=0){
				new loginQuery(this){
					@Override
					//로그인 통신이 시작되기 전에 값설정
					public void getInformation() {
						this.setID(ID);
						this.setPassword(pass);
					}
					@Override
					//로그인 통신이 끝났을때
					public void postprocess() {
						try{
							JSONObject json = new JSONObject(this.getResult());
							//mNo를 얻는다. 이것이  -1이라면 패스워드 -2라면 잘못된 ID이다.
							int mNo = json.getInt("mNo");
							String ResultMessage = "";
							switch(mNo){
								case -1: ResultMessage = getResources().getString(R.string.TEXT_LOGIN_RESULT_WRONG_PASSWORD); break;
								case -2: ResultMessage = getResources().getString(R.string.TEXT_LOGIN_RESULT_WRONG_ID); break;
								default:{
									//양수면 authCode를 받고 Toast메세지 환영메세지를 띄우면서 user를 만든다.
									String authCode = json.getString("authCode");
									ResultMessage = json.getString("name")+getResources().getString(R.string.TEXT_LOGIN_WELCOME);
									user = new UserInfo(mNo, authCode);
									//authCode로 화면을 구성한다.
									settingScreen();
								}
								//음수라면 다시 로그인 엑티비티로
								if(mNo<0) ToLogin();
								Toast.makeText(MainActivity.this, ResultMessage, Toast.LENGTH_LONG).show();
							}
						}catch(JSONException e){
							e.printStackTrace();
						}
					}
					
				}.start();
			}else{
				//저장된 정보가 없으면 로그인 엑티비티로
				ToLogin();
			}
		} else {
			//user가 null이 아니면 화면 구성
			settingScreen();
		}
	}
	public void settingScreen(){
		setContentView(R.layout.activity_main);
		//user정보로 화면 구성
		TableLayout table = (TableLayout)findViewById(R.id.titleMenuTable);
		TableLayout.LayoutParams tlp = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
		tlp.bottomMargin = 60;
		
		if(user.canEnroll()) table.addView(MenuRow(getResources().getString(R.string.TEXT_KEY_ENROLL)), tlp);
		if(user.canInquiry()) table.addView(MenuRow(getResources().getString(R.string.TEXT_KEY_INQUIRY)), tlp);
		if(user.canExport()) table.addView(MenuRow(getResources().getString(R.string.TEXT_KEY_EXPORT)), tlp);
		if(user.canImport()) table.addView(MenuRow(getResources().getString(R.string.TEXT_KEY_IMPORT)), tlp);
	}
	private TableRow MenuRow(String Menu){
		TableRow row = new TableRow(this);
		
		TableRow.LayoutParams rlp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
		rlp.height = 75;

		Button menuBut = new Button(this);
		menuBut.setWidth(300);
		menuBut.setHeight(2);
		if(Menu.compareTo(getResources().getString(R.string.TEXT_KEY_ENROLL))==0) EnrollBtn = menuBut;
		else if(Menu.compareTo(getResources().getString(R.string.TEXT_KEY_INQUIRY))==0)	Inquiry = menuBut;
		else if(Menu.compareTo(getResources().getString(R.string.TEXT_KEY_EXPORT))==0)	Export = menuBut;
		else if(Menu.compareTo(getResources().getString(R.string.TEXT_KEY_IMPORT))==0)	Import = menuBut;
		
		menuBut.setText(Menu);
		menuBut.setBackgroundResource(R.color.red);
		menuBut.setTextColor(Color.WHITE);
		row.addView(menuBut, rlp);
		menuBut.setOnClickListener(butListener);
		
		return row;
	}
	public void ToLogin(){
		Intent intent = new Intent(this, LoginActivity.class);
		this.startActivityForResult(intent, REQUEST_CODE);
	}
	//로그인 엑티비티에서 돌아왔을때
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode){
			case REQUEST_CODE:{
				if(resultCode==Activity.RESULT_OK){
					//mNo, authCode, ID, 패스워드를 받아
					int mNo = data.getIntExtra("mNo", -1);
					String authCode = data.getStringExtra("authCode");
					String ID = data.getStringExtra("ID");
					String password = data.getStringExtra("password");
					
					//ID와 비밀번호는 sharedPreference에 저장
					SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
					Editor editor = prefs.edit();
					editor.putString("ID", ID);
					editor.putString("Pass", password);
					editor.commit();
					
					//mNo, authCode로 유저정보 생성
					user = new UserInfo(mNo, authCode);
					
					//화면 구성
					settingScreen();
				}
				break;
			}
		}
	}
	
	//ButtonListener
	private OnClickListener butListener = new OnClickListener(){
		public void onClick(View v) {
			Button clicked = (Button)v;
			Intent intent = null;
			if(clicked.equals(EnrollBtn)){
				intent = new Intent(MainActivity.this, EnrollActivity.class);
			} else if(clicked.equals(Inquiry)){
				intent = new Intent(MainActivity.this, InquiryActivity.class);
			} else if(clicked.equals(Export)){
				intent = new Intent(MainActivity.this, ExportActivity.class);
			} else if(clicked.equals(Import)){
				intent = new Intent(MainActivity.this, ImportActivity.class);
			} else {
				//TODO Exception Handling
				return;
			}
			startActivity(intent);
		}
	};
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		// 로그아웃 추가
		menu.add(0, 1, 0, getResources().getString(R.string.TEXT_KEY_LOGOUT));
		return true;
	}
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
			case 1:{
				//로그아웃시에 sharedpreference를 지우고 
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
				Editor editor = prefs.edit();
				editor.putString("ID", "");
				editor.putString("Pass", "");
				editor.commit();
				user=null;
				
				//Toast를 띄우고
				Toast.makeText(MainActivity.this, getResources().getString(R.string.TEXT_LOGOUT_RESULT_SUCCESS), Toast.LENGTH_LONG).show();
				
				//로그인 엑티비티로
				ToLogin();
				return true;
			}
		}
		return false;
	}
}
