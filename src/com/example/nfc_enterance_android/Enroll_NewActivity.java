package com.example.nfc_enterance_android;

import java.util.Calendar;

import com.example.nfc_enterance_android.DBModule.insertQuery;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

public class Enroll_NewActivity extends Activity {
	EditText SerialText;
	EditText NameText;
	EditText PurchaseDateText;
	EditText PriceText;
	EditText Manger1DText;
	EditText Manger1Text;
	EditText SectionText;
	EditText ClassText;
	Button SubmitBtn;
	
	final static int REQUEST_CODE = 1;
	boolean DatePickerEnable = true;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    this.setContentView(R.layout.reg_new_activity);
	    
	    SerialText=(EditText)findViewById(R.id.serialNumberEntry);
		NameText=(EditText)findViewById(R.id.nameEntry);
		PurchaseDateText=(EditText)findViewById(R.id.purchaseDateEntry);
		PriceText=(EditText)findViewById(R.id.priceEntry);
		Manger1DText=(EditText)findViewById(R.id.mainManagerEntry);
		Manger1Text=(EditText)findViewById(R.id.viceManagerEntry);
		SectionText=(EditText)findViewById(R.id.departmentEntry);
		ClassText=(EditText)findViewById(R.id.categoryEntry);
		
		SubmitBtn=(Button)findViewById(R.id.newRegOkButton);
		
		PurchaseDateText.setOnTouchListener(touch);
		
		SubmitBtn.setOnClickListener(butListener);
	}
	
	private OnTouchListener touch = new OnTouchListener(){
		public boolean onTouch(View arg0, MotionEvent arg1) {
			EditText touched = (EditText)arg0;
			if(touched.equals(PurchaseDateText)&&DatePickerEnable){
				DatePickerEnable=false;
				
				Calendar c = Calendar.getInstance();
				int cyear = c.get(Calendar.YEAR);
				int cmonth = c.get(Calendar.MONTH);
				int cday = c.get(Calendar.DAY_OF_MONTH);
				
				final DatePickerDialog alert = new DatePickerDialog(Enroll_NewActivity.this, null, cyear, cmonth, cday);
				alert.setCancelable(true);
				alert.setCanceledOnTouchOutside(true);
				alert.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.TEXT_KEY_OK),
						new DialogInterface.OnClickListener(){
							public void onClick(DialogInterface dialog, int which) {
								DatePicker picker = alert.getDatePicker();
								String Date =picker.getYear()+"-"+(picker.getMonth()+1)+"-"+picker.getDayOfMonth();
								PurchaseDateText.setText(Date);
								DatePickerEnable = true;
							}});
				alert.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.TEXT_KEY_CANCLE),
						new DialogInterface.OnClickListener(){
							public void onClick(DialogInterface dialog, int which) {
								alert.dismiss();
								DatePickerEnable = true;
					}});
				alert.show();
			}
			return true;
		}
	};
	private String parseResultCode(String ResultCodeString){
		int ResultCode = Integer.valueOf(ResultCodeString);
		String ResultMessage = null;
		switch(ResultCode){
			case 0: ResultMessage = getResources().getString(R.string.TEXT_ENROLL_RESULT_SUCCESS); break;
			default: ResultMessage = getResources().getString(R.string.TEXT_RESULT_UNKNOWN); break;
		}
		
		return ResultMessage;
	}
	private OnClickListener butListener = new OnClickListener(){
		public void onClick(View v) {
			Button clicked = (Button)v;
			if(clicked.equals(SubmitBtn)){
				if(CheckInput()){
					Intent intent = new Intent(Enroll_NewActivity.this, NFCDetectActivity.class);
					Enroll_NewActivity.this.startActivityForResult(intent, REQUEST_CODE);
				}
			} else {
				//TODO Exception Handling
				return;
			}
		}
	};
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode){
			case REQUEST_CODE:{
				if(resultCode==Activity.RESULT_OK){
					final long NfcId = data.getLongExtra("nfcID", 0);
					new insertQuery(Enroll_NewActivity.this){
						public void getInformation() {
							String Name = NameText.getText().toString();
							String Serial =SerialText.getText().toString();
							String PurchaseDate = PurchaseDateText.getText().toString();
							int Price = Integer.valueOf(PriceText.getText().toString());
							String Manger1D = Manger1DText.getText().toString();
							String Manger1 = Manger1Text.getText().toString();
							String Section = SectionText.getText().toString();
							String Class = ClassText.getText().toString();
							
							this.setuNo(NfcId);
							this.setuName(Name);
							this.setSerialNumber(Serial);
							this.setPurchaseDate(PurchaseDate);
							this.setPrice(Price);
							this.setmanager(Manger1D);
							this.setSub_manager(Manger1);
							this.setpart(Section);
							this.setCategory(Class);
						}
						public void postprocess() {
							AlertDialog.Builder builder = new AlertDialog.Builder(Enroll_NewActivity.this);
							builder.setMessage(parseResultCode(this.getResult())).setPositiveButton(getResources().getString(R.string.TEXT_KEY_OK), new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									Enroll_NewActivity.this.finish();					
								}
							});
							builder.create().show();
						}
					}.start();
				}
				break;
			}
		}
	}
	private boolean CheckInput(){
		boolean filled = true;
		
		return filled;
	}

}
